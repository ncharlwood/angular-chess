# AngularChess
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.2.

I also added a very basic nodeJS backend (that doesn't check the moves are legal chess moves, but I'll make it a bit nicer soon :) ). Just flip the local bool in `game.component.ts` if you want to muck around before its properly ready 

## Development servers

For Angular frontend, run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

For NodeJS backend, run `npm run server:dev` for a dev server running on port 1337. The app will automatically reload if you change any of the source files.

## Build (not adjusted for backend)

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests (not adjusted for backend)

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Upcoming
- Tidy up UI design
- Make responsive UI
- Add some kind of lobby system with game codes or something
- Keep track of game state in backend and validate moves
- Work out how to deploy this
- Optimize the game logic
