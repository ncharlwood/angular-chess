import { Component, Output, Input, EventEmitter } from "@angular/core";
import { SquareColour, Coords, Board } from "src/app/types.model";

@Component({
  selector: "app-board",
  template: `
    <table class="Board">
      <tr *ngFor="let row of board; let i = index">
        <td
          *ngFor="let col of row; let j = index"
          class="Square"
          [ngClass]="{
            'Square-Dark': getColour(i, j) === SquareColour.DARK,
            'Square-Light': getColour(i, j) === SquareColour.LIGHT,
            'Square-Selected': isSelected(i, j),
            'Square-Valid': isValidSquare(i, j)
          }"
          (click)="handleClick(i, j)"
        >
          <app-piece [piece]="board[i][j]"></app-piece>
        </td>
      </tr>
    </table>
  `,
  styleUrls: ["./board.component.scss"],
})
export class BoardComponent {
  SquareColour = SquareColour;

  @Input()
  board: Board;

  @Input()
  selectedSquare: Coords;

  @Input()
  validMoves: ReadonlyArray<Coords>;

  @Output()
  select = new EventEmitter<Coords>();

  handleClick(row: number, col: number) {
    this.select.emit({ row, col });
  }

  // TODO - make this work when you click on a piece too
  isSelected(row: number, col: number): boolean {
    return row === this.selectedSquare?.row && col === this.selectedSquare?.col;
  }

  isValidSquare(row: number, col: number): boolean {
    return !!this.validMoves?.find((x) => x.row === row && x.col === col);
  }

  getColour(row: number, col: number): SquareColour {
    if (
      (this.isEven(row) && this.isEven(col)) ||
      (!this.isEven(row) && !this.isEven(col))
    ) {
      return SquareColour.LIGHT;
    } else {
      return SquareColour.DARK;
    }
  }

  isEven(n: number) {
    return n % 2 === 0;
  }
}
