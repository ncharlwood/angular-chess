import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-game-load",
  template: `
    <div class="Card">
      <p>An existing game has been found</p>
      <button (click)="load()">Load</button>
      <button (click)="discard()">Discard</button>
    </div>
  `,
  styleUrls: ["./game-load.component.scss"],
})
export class GameLoadComponent {
  @Output()
  loadGame = new EventEmitter<void>();

  @Output()
  discardGame = new EventEmitter<void>();

  load() {
    this.loadGame.emit();
  }

  discard() {
    this.discardGame.emit();
  }
}
