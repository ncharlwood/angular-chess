import { Component, Output, EventEmitter, Input } from "@angular/core";
import { PieceType, EndCondition, Side } from "src/app/types.model";

@Component({
  selector: "app-game-info",
  template: `
    <div class="Card">
      <p>{{ turn | titlecase }}'s turn</p>
      <p *ngIf="!safeMove && endCondition === EndCondition.CHECK">
        This move leaves you in check. Try again
      </p>
      <p *ngIf="!safeMove && endCondition === EndCondition.NONE">
        This move puts you in check. Try again
      </p>
      <p *ngIf="endCondition != EndCondition.NONE">
        {{ endCondition | titlecase }}
      </p>
      <button
        *ngIf="
          endCondition === EndCondition.CHECKMATE ||
          endCondition === EndCondition.STALEMATE
        "
        class="Reset"
        (click)="newGame()"
      >
        Restart
      </button>
      <ng-container *ngIf="canUpgrade">
        <label for="upgrade">
          Please pick a piece to upgrade your pawn into:
        </label>
        <select id="upgrade" #upgrade>
          <option value="2">Rook</option>
          <option value="3">Knight</option>
          <option value="4">Bishop</option>
          <option value="5">Queen</option>
        </select>
        <button (click)="upgradePawn(upgrade.value)">Select Piece</button>
      </ng-container>
    </div>
  `,
  styleUrls: ["./game-info.component.scss"],
})
export class GameInfoComponent {
  EndCondition = EndCondition;

  @Input()
  turn: Side;

  @Input()
  safeMove: boolean;

  @Input()
  endCondition: EndCondition;

  @Input()
  canUpgrade: boolean;

  @Output()
  reset = new EventEmitter<void>();

  @Output()
  upgrade = new EventEmitter<PieceType>();

  newGame() {
    this.reset.emit();
  }

  upgradePawn(pieceStr: string) {
    const piece = parseInt(pieceStr, 10) as PieceType;
    this.upgrade.emit(piece);
  }
}
