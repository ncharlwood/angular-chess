import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { getSideFromPiece } from "src/app/game-helpers";
import { PieceType, Side } from "src/app/types.model";

@Component({
  selector: "app-captured-pieces",
  template: `
    <div class="Container">
      <app-piece
        class="CapturedPiece"
        *ngFor="let piece of white"
        [piece]="piece"
      ></app-piece>
    </div>
    <div class="Container CapturedBlack">
      <app-piece
        class="CapturedPiece"
        *ngFor="let piece of black"
        [piece]="piece"
      ></app-piece>
    </div>
  `,
  styleUrls: ["./captured-pieces.component.scss"],
})
export class CapturedPiecesComponent implements OnChanges {
  @Input() pieces: ReadonlyArray<PieceType>;

  white: ReadonlyArray<PieceType>;
  black: ReadonlyArray<PieceType>;

  ngOnChanges() {
    this.white = this.pieces.filter(
      (piece) => getSideFromPiece(piece) === Side.WHITE
    );
    this.black = this.pieces.filter(
      (piece) => getSideFromPiece(piece) === Side.BLACK
    );
  }
}
