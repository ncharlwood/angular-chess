import { Component, OnInit } from "@angular/core";
import {
  Coords,
  EndCondition,
  GamePhase,
  Piece,
  PieceType,
  GameState,
  Side,
  SquareColour,
} from "src/app/types.model";

import {
  isPieceOwner,
  isMoveSafe,
  getPieceFromBoardCoords,
  makeMoveAndUpdateState,
  getNewState,
  getValidMovesIncludingInPassingAndCastling,
  upgradePawn,
} from "src/app/game-helpers";
import { WebsocketService } from "../services/websocket.service";

@Component({
  selector: "app-game",
  template: `
    <div class="Container">
      <div class="Primary">
        <app-board
          [ngClass]="{ Disabled: shouldDisableBoard() }"
          [board]="state.board"
          [selectedSquare]="selectedPiece?.coords"
          [validMoves]="validMoves"
          (select)="handleClick($event)"
        ></app-board>
        <app-game-load
          *ngIf="savedGame"
          (loadGame)="loadGame(true)"
          (discardGame)="newGame()"
        ></app-game-load>
        <app-game-info
          [ngClass]="{ Disabled: savedGame }"
          [turn]="state.turn"
          [safeMove]="safeMove"
          [endCondition]="state.endCondition"
          [canUpgrade]="canUpgradePawn()"
          (reset)="newGame()"
          (upgrade)="handlePromotion($event)"
        ></app-game-info>
      </div>
      <app-captured-pieces
        *ngIf="state.capturedPieces.length > 0"
        class="Card Pieces Container"
        [ngClass]="{ Disabled: savedGame }"
        [pieces]="state.capturedPieces"
      ></app-captured-pieces>
    </div>
  `,
  styleUrls: ["./game.component.scss"],
})
export class GameComponent implements OnInit {
  SquareColour = SquareColour;
  EndState = EndCondition;

  state: GameState;

  phase: GamePhase;
  safeMove: boolean;

  selectedPiece: Piece;
  validMoves: ReadonlyArray<Coords>;

  savedGame: GameState;
  key = "game";

  local = true;
  playerSide: Side;

  constructor(private websocketService: WebsocketService) {}

  ngOnInit() {
    const savedGame = JSON.parse(localStorage.getItem(this.key));

    if (!this.local) {
      this.websocketService.joinRoom("ABCD");

      this.websocketService.connect$.pipe().subscribe((connection) => {
        this.playerSide = connection.colour;
      });

      this.websocketService.moves$.pipe().subscribe((move) => {
        this.state = makeMoveAndUpdateState(
          this.state,
          move.piece,
          move.toSquare
        );
      });

      this.websocketService.upgrade$.pipe().subscribe((upgrade) => {
        this.state = upgradePawn(this.state, upgrade.upgrade);
      });
    }

    if (this.local && savedGame) {
      this.savedGame = savedGame;
      this.loadGame();
    } else {
      this.newGame();
    }
  }

  newGame() {
    this.state = getNewState();
    this.phase = GamePhase.PIECE_SELECT;
    this.safeMove = true;

    this.discardGame();
  }

  loadGame(discard = false) {
    this.state = this.savedGame;
    this.phase = GamePhase.PIECE_SELECT;
    this.safeMove = true;

    if (discard) {
      this.discardGame(false);
    }
  }

  shouldDisableBoard(): boolean {
    return (
      !!this.savedGame ||
      this.state.upgrade?.canUpgrade ||
      this.state.endCondition === EndCondition.CHECKMATE ||
      this.state.endCondition === EndCondition.STALEMATE
    );
  }

  canUpgradePawn(): boolean {
    return (
      (this.local && this.state.upgrade?.canUpgrade) ||
      (!this.local &&
        this.state.turn === this.playerSide &&
        this.state.upgrade?.canUpgrade)
    );
  }

  handleClick(clickedSquare: Coords) {
    const { row, col } = clickedSquare;

    if (!this.local && this.state.turn !== this.playerSide) {
      return;
    }

    switch (this.phase) {
      // player has clicked on a square
      case GamePhase.PIECE_SELECT: {
        // only do stuff if clicking on a piece and the player owns the piece
        if (
          this.state.board[row][col] &&
          isPieceOwner(this.state.board[row][col], this.state.turn)
        ) {
          const piece = getPieceFromBoardCoords(
            this.state.board,
            clickedSquare
          );
          this.selectedPiece = piece;

          // calculate valid moves for selected piece using previous board
          const moves = getValidMovesIncludingInPassingAndCastling(
            piece,
            this.state
          );
          if (moves) {
            this.validMoves = moves;
            this.phase = GamePhase.MOVE_SELECT;
          }
        }
        break;
      }

      // player has clicked on a square to move to
      case GamePhase.MOVE_SELECT: {
        const validMove = this.validMoves.find(
          (x) => x.row === row && x.col === col
        );
        const isSafeMove = validMove
          ? isMoveSafe(this.state, this.selectedPiece, clickedSquare)
          : false;

        if (validMove && isSafeMove) {
          this.safeMove = true;
          if (!this.local) {
            this.websocketService.makeMove(this.selectedPiece, clickedSquare);
          } else {
            this.state = makeMoveAndUpdateState(
              this.state,
              this.selectedPiece,
              clickedSquare
            );
          }
        } else if (validMove && !isSafeMove) {
          this.safeMove = false;
        }

        this.selectedPiece = undefined;
        this.validMoves = undefined;
        this.phase = GamePhase.PIECE_SELECT;
        break;
      }
    }

    if (this.local) {
      this.saveGame();
    }
  }

  handlePromotion(piece: PieceType) {
    if (this.local) {
      this.state = upgradePawn(this.state, piece);
    } else {
      this.websocketService.upgradePawn(piece);
    }
  }

  saveGame() {
    localStorage.setItem(this.key, JSON.stringify(this.state));
  }

  discardGame(deleteSave = true) {
    this.savedGame = null;
    if (deleteSave) {
      localStorage.removeItem(this.key);
    }
  }

  logState() {
    console.log("board", this.state.board);
    console.log("turn", this.state.turn);
    console.log("endCondition", this.state.endCondition);
    console.log("capturedPieces", this.state.capturedPieces);
    console.log("castlingOptions", this.state.castlingOptions);
    console.log("inPassingChance", this.state.inPassingChance);
    console.log("upgrade", this.state.upgrade);
  }
}
