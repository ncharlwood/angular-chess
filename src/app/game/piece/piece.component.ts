import { Component, OnInit, Input } from "@angular/core";
import { PieceType } from "src/app/types.model";

@Component({
  selector: "app-piece",
  template: `
    <p
      class="Piece"
      [ngClass]="{ 'Piece-Black': piece > 0, 'Piece-White': piece < 0 }"
      [innerHTML]="displayPiece"
    ></p>
  `,
  styleUrls: ["./piece.component.scss"],
})
export class PieceComponent implements OnInit {
  @Input()
  piece: number;

  displayPiece: string;

  constructor() {}

  ngOnInit() {
    const pieceMap = {
      [PieceType.PAWN_WHITE]: "&#9817",
      [PieceType.ROOK_WHITE]: "&#9814",
      [PieceType.KNIGHT_WHITE]: "&#9816",
      [PieceType.BISHOP_WHITE]: "&#9815",
      [PieceType.QUEEN_WHITE]: "&#9813",
      [PieceType.KING_WHITE]: "&#9812",
      [PieceType.PAWN_BLACK]: "&#9823",
      [PieceType.ROOK_BLACK]: "&#9820",
      [PieceType.KNIGHT_BLACK]: "&#9822",
      [PieceType.BISHOP_BLACK]: "&#9821",
      [PieceType.QUEEN_BLACK]: "&#9819",
      [PieceType.KING_BLACK]: "&#9818",
    };

    this.displayPiece = pieceMap[this.piece];
  }
}
