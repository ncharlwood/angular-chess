import { Injectable } from "@angular/core";
import { webSocket, WebSocketSubject } from "rxjs/webSocket";
import { filter } from "rxjs/operators";
import { Coords, Piece, PieceType } from "../types.model";
import {
  isConnectMessage,
  isMoveMessage,
  isUpgradeMessage,
  JoinRoomMessage,
  Message,
  MessageType,
  MoveMessage,
  UpgradeMessage,
} from "./message.types";

@Injectable({
  providedIn: "root",
})
export class WebsocketService {
  private _webSocketSubject$: WebSocketSubject<Message> = webSocket(
    "ws://127.0.0.1:1337"
  );

  get messages$() {
    return this._webSocketSubject$.asObservable();
  }

  get moves$() {
    return this.messages$.pipe(filter(isMoveMessage));
  }

  get connect$() {
    return this.messages$.pipe(filter(isConnectMessage));
  }

  get upgrade$() {
    return this.messages$.pipe(filter(isUpgradeMessage));
  }

  private _sendMessage(message: Message) {
    console.log(message);
    this._webSocketSubject$.next(message);
  }

  joinRoom(code: string) {
    const msg: JoinRoomMessage = {
      type: MessageType.JOIN_ROOM,
      code,
    };
    this._sendMessage(msg);
  }

  makeMove(piece: Piece, to: Coords) {
    const msg: MoveMessage = {
      type: MessageType.MOVE,
      piece,
      toSquare: to,
    };
    this._sendMessage(msg);
  }

  upgradePawn(upgrade: PieceType) {
    const msg: UpgradeMessage = {
      type: MessageType.UPGRADE,
      upgrade,
    };
    this._sendMessage(msg);
  }
}
