import { Coords, Piece, PieceType, Side } from "../types.model";

export enum MessageType {
  MOVE = "move",
  CONNECT = "connect",
  UPGRADE = "upgrade",
  JOIN_ROOM = "join_room",
}

export interface MoveMessage {
  type: MessageType.MOVE;
  piece: Piece;
  toSquare: Coords;
}

export interface ConnectMessage {
  type: MessageType.CONNECT;
  colour: Side;
}

export interface JoinRoomMessage {
  type: MessageType.JOIN_ROOM;
  code: string;
}

export interface UpgradeMessage {
  type: MessageType.UPGRADE;
  upgrade: PieceType;
}

export type Message =
  | MoveMessage
  | ConnectMessage
  | UpgradeMessage
  | JoinRoomMessage;

export function isMoveMessage(message: Message): message is MoveMessage {
  return message.type === MessageType.MOVE;
}

export function isConnectMessage(message: Message): message is ConnectMessage {
  return message.type == MessageType.CONNECT;
}

export function isUpgradeMessage(message: Message): message is UpgradeMessage {
  return message.type == MessageType.UPGRADE;
}

export function isJoinRoomMessage(
  message: Message
): message is JoinRoomMessage {
  return message.type == MessageType.JOIN_ROOM;
}
