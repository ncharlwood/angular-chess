export enum GamePhase {
  PIECE_SELECT = "piece-select",
  MOVE_SELECT = "move-select",
}

export enum Side {
  WHITE = "white",
  BLACK = "black",
}

export enum SquareColour {
  DARK = "dark",
  LIGHT = "light",
}

export type Board = ReadonlyArray<ReadonlyArray<number>>;

export interface Piece {
  type: PieceType;
  coords: Coords;
}

export interface Coords {
  row: number;
  col: number;
}

export interface Move {
  distance: number;
  type: Movement;

  // only for pawns
  onlyForward?: boolean; // unused
  onlyAttack?: boolean;
  noAttack?: boolean;

  // only for knights
  canJump?: boolean; // unused
}

export enum Movement {
  STRAIGHT = "straight",
  ORTHOGONAL = "orthogonal",
  DIAGONAL = "diagonal",
  L_SHAPE = "l-shape",
}

export interface Direction {
  plusRow?: boolean;
  plusCol?: boolean;
}

export enum PieceType {
  PAWN_BLACK = 1,
  ROOK_BLACK = 2,
  KNIGHT_BLACK = 3,
  BISHOP_BLACK = 4,
  QUEEN_BLACK = 5,
  KING_BLACK = 6,

  PAWN_WHITE = -1,
  ROOK_WHITE = -2,
  KNIGHT_WHITE = -3,
  BISHOP_WHITE = -4,
  QUEEN_WHITE = -5,
  KING_WHITE = -6,
}

export interface InPassingChance {
  type: PieceType;
  attackSquare: Coords;
  pieceSquare: Coords;
}

export interface UpgradeInfo {
  canUpgrade: boolean;
  coords: Coords;
}

export enum EndCondition {
  NONE = "none",
  CHECK = "check",
  CHECKMATE = "checkmate",
  STALEMATE = "stalemate",
}

export interface GameState {
  board: Board;
  turn: Side;
  endCondition: EndCondition;
  capturedPieces: ReadonlyArray<PieceType>;
  castlingOptions: CastlingOptions;
  inPassingChance?: InPassingChance;
  upgrade?: UpgradeInfo;
}

/**
 * onlyAttacks - use this when you only want moves that can capture a piece
 *
 * friendlyFire - use this when checking kings valid moves, prevents king from taking a piece without escaping check
 *
 * throughKing - use this when checking for attacks on the king, prevents the squares 'behind' the king from being deemed safe
 *
 * pawnAttack - use this when you want pawn attacks to be included as valid moves
 */
export interface SweepModes {
  onlyAttacks: boolean;
  friendlyFire: boolean;
  throughKing: boolean;
  pawnAttack: boolean;
}

export interface CastlingPair {
  king: Coords;
  rook: Coords;
}

export interface CastlingOptions {
  black: Array<CastlingPair>;
  white: Array<CastlingPair>;
}
