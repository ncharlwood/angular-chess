import {
  Board,
  CastlingOptions,
  CastlingPair,
  Coords,
  Direction,
  EndCondition,
  GameState,
  InPassingChance,
  Move,
  Movement,
  Piece,
  PieceType,
  Side,
  SweepModes,
} from "./types.model";
import { flatten } from "@angular/compiler";

export function getNewState(): GameState {
  const board = [
    [2, 3, 4, 5, 6, 4, 3, 2], // black
    [1, 1, 1, 1, 1, 1, 1, 1], // black
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, -1, -1, -1, -1], // white
    [-2, -3, -4, -5, -6, -4, -3, -2], // white
  ];

  return {
    board,
    endCondition: EndCondition.NONE,
    turn: Side.WHITE,
    capturedPieces: [],
    inPassingChance: undefined,
    upgrade: undefined,
    castlingOptions: getStartingCastlingOptions(),
  };
}

export function getStartingCastlingOptions(): CastlingOptions {
  return {
    black: [
      {
        king: { row: 0, col: 4 },
        rook: { row: 0, col: 0 },
      },
      {
        king: { row: 0, col: 4 },
        rook: { row: 0, col: 7 },
      },
    ],
    white: [
      {
        king: { row: 7, col: 4 },
        rook: { row: 7, col: 0 },
      },
      {
        king: { row: 7, col: 4 },
        rook: { row: 7, col: 7 },
      },
    ],
  };
}

/**
 *  Checks for check/checkmate/stalemate:
 *  - check if king is being attacked
 *  - checkmate if king is being attacked
 *     and the king has no safe moves
 *     and the attacker cant be blocked or taken
 *  - stalemate if the player has no safe moves
 *     but the kings square is safe
 */
export function checkEndState(board: Board, side: Side): EndCondition {
  // conditions for check/checkmate/stalemate
  let kingAttacked = false;
  let kingCanMove = false;
  let canBlockAttacker = false;
  let hasLegalMoves = true;

  // pretend king can move as other pieces and see if it can hypothetically capture anything
  // if it can capture a piece, it is being attacked
  const kingCoords = findKing(board, side);
  const kingPiece: Piece = {
    type: board[kingCoords.row][kingCoords.col],
    coords: kingCoords,
  };
  const opponentPieceTypes: Array<PieceType> = getPieceTypes(
    board,
    getOtherSide(side)
  );
  const attacksOnKing: Array<Array<Coords>> = [];

  // check if king is attacked
  opponentPieceTypes.map((type) => {
    // construct fake peice with coords of king and movement of an opponent piece
    const piece: Piece = { type: -1 * type, coords: kingCoords };
    const kingModes: SweepModes = {
      onlyAttacks: true, // avoids a pawn directly in front of the king from being deemed an attacker
      friendlyFire: false,
      throughKing: false,
      pawnAttack: true,
    };
    const attack = flatten(getValidMoveCoords(piece, board, kingModes));

    // see if it can capture anything and it matches the piece type we are checking
    if (attack.length > 0 && attack.find((x) => board[x.row][x.col] === type)) {
      attacksOnKing.push(attack);
      kingAttacked = true;
    }
  });

  let modes: SweepModes = {
    onlyAttacks: false,
    friendlyFire: true,
    throughKing: true,
    pawnAttack: true,
  };
  const opponentMoves = allMoves(board, getOtherSide(side), false, modes);

  // filter out kings unsafe moves
  // find cases where king move overlaps with a potential opponent move
  let kingMoves: Array<Coords> = flatten(getValidMoveCoords(kingPiece, board));
  kingMoves = kingMoves.filter(
    (move) => !opponentMoves.find((x) => compareCoords(x, move))
  );

  if (kingMoves.length > 0) {
    kingCanMove = true;
  }

  // check if we can block the attack on the king,
  // but dont bother checking the kings moves since the king cant block
  modes = {
    onlyAttacks: false,
    friendlyFire: false,
    throughKing: false,
    pawnAttack: false,
  };
  const ourMoves: Array<Coords> = allMoves(board, side, true, modes);

  // we can only block one attack on the king,
  // if there are more attacks we cant block them all, so dont even bother
  let blockingMoves: Array<Coords> = [];
  if (attacksOnKing.length === 1) {
    blockingMoves = (flatten(attacksOnKing) as Array<Coords>).filter(
      (attackSquare) =>
        !!ourMoves.find((move) => compareCoords(move, attackSquare))
    );

    if (blockingMoves.length > 0) {
      canBlockAttacker = true;
    }
  }

  // combine our legal kings moves with all other peices moves
  // and see if we have any legal moves to make
  const ourMovesAll: Array<Coords> = ourMoves.concat(kingMoves);
  if (ourMovesAll.length === 0) {
    hasLegalMoves = false;
  }

  // TODO make a function to return the bools and another to determine the gamestate given the bools
  // this makes it a bit easier to test why the end state is calculate incorrectly
  if (kingAttacked && !kingCanMove && !canBlockAttacker) {
    return EndCondition.CHECKMATE;
  } else if (kingAttacked && (kingCanMove || canBlockAttacker)) {
    return EndCondition.CHECK;
  } else if (!kingAttacked && !hasLegalMoves) {
    return EndCondition.STALEMATE;
  }
  return EndCondition.NONE;
}

/**
 *  Get every move a given player can make. Does not include castling
 */
export function allMoves(
  board: Board,
  side: Side,
  skipKing: boolean,
  modes?: SweepModes
): Array<Coords> {
  let moves: Array<Array<Coords>> = [];
  board.map((rowArray, row) =>
    rowArray.map((square, col) => {
      // square is occupied and the piece is owned by the given player
      if (square && isPieceOwner(square, side)) {
        // get moves for the piece, unless the current piece is a king and we are supposed to skip the king
        if (
          !(
            skipKing &&
            (square === PieceType.KING_BLACK || square === PieceType.KING_WHITE)
          )
        ) {
          const piece: Piece = {
            type: square,
            coords: { row, col },
          };
          moves = moves.concat(getValidMoveCoords(piece, board, modes));
        }
      }
    })
  );
  return flatten(moves);
}

export function pawnFirstMove(piece: Piece): boolean {
  return (
    (piece.type === PieceType.PAWN_BLACK && piece.coords.row === 1) ||
    (piece.type === PieceType.PAWN_WHITE && piece.coords.row === 6)
  );
}

/**
 *  Retrieve types of motion for a piece type
 */
export function getMoves(piece: Piece): Array<Move> {
  switch (piece.type) {
    case PieceType.PAWN_WHITE:
    case PieceType.PAWN_BLACK: {
      return [
        {
          distance: pawnFirstMove(piece) ? 2 : 1,
          type: Movement.STRAIGHT,
          onlyForward: true,
          noAttack: true,
        },
        {
          distance: 1,
          type: Movement.DIAGONAL,
          onlyForward: true,
          onlyAttack: true,
        },
      ];
    }
    case PieceType.ROOK_WHITE:
    case PieceType.ROOK_BLACK: {
      return [
        {
          distance: 8,
          type: Movement.ORTHOGONAL,
        },
      ];
    }
    case PieceType.KNIGHT_WHITE:
    case PieceType.KNIGHT_BLACK: {
      return [
        {
          distance: 1,
          type: Movement.L_SHAPE,
          canJump: true,
        },
      ];
    }
    case PieceType.BISHOP_WHITE:
    case PieceType.BISHOP_BLACK: {
      return [
        {
          distance: 8,
          type: Movement.DIAGONAL,
        },
      ];
    }
    case PieceType.QUEEN_WHITE:
    case PieceType.QUEEN_BLACK: {
      return [
        {
          distance: 8,
          type: Movement.ORTHOGONAL,
        },
        {
          distance: 8,
          type: Movement.DIAGONAL,
        },
      ];
    }
    case PieceType.KING_WHITE:
    case PieceType.KING_BLACK: {
      return [
        {
          distance: 1,
          type: Movement.ORTHOGONAL,
        },
        {
          distance: 1,
          type: Movement.DIAGONAL,
        },
      ];
    }
  }
}

/**
 *  Given a board and a piece, get the coordinates the piece can legally move to
 */
export function getValidMoveCoords(
  piece: Piece,
  board: Board,
  modes?: SweepModes
): Array<Array<Coords>> {
  const moveTypes = getMoves(piece);
  let validCoords: Array<Array<Coords>> = [];

  moveTypes.forEach((move) => {
    switch (move.type) {
      case Movement.STRAIGHT: {
        validCoords.push(
          sweep(
            piece,
            board,
            move,
            { plusRow: piece.type > 0 ? true : false },
            modes
          )
        );
        break;
      }
      case Movement.ORTHOGONAL: {
        validCoords.push(sweep(piece, board, move, { plusRow: false }, modes));
        validCoords.push(sweep(piece, board, move, { plusCol: true }, modes));
        validCoords.push(sweep(piece, board, move, { plusRow: true }, modes));
        validCoords.push(sweep(piece, board, move, { plusCol: false }, modes));
        break;
      }
      case Movement.DIAGONAL: {
        if (!(piece.type === PieceType.PAWN_WHITE)) {
          validCoords.push(
            sweep(piece, board, move, { plusRow: true, plusCol: false }, modes)
          );
          validCoords.push(
            sweep(piece, board, move, { plusRow: true, plusCol: true }, modes)
          );
        }
        if (!(piece.type === PieceType.PAWN_BLACK)) {
          validCoords.push(
            sweep(piece, board, move, { plusRow: false, plusCol: true }, modes)
          );
          validCoords.push(
            sweep(piece, board, move, { plusRow: false, plusCol: false }, modes)
          );
        }
        break;
      }
      case Movement.L_SHAPE: {
        validCoords = validCoords.concat(lMovement(piece, board, modes));
      }
    }
  });

  return validCoords.filter((moves) => moves.length > 0);
}

/**
 *  Given a board and a piece, get the coordinates the piece can move to using l-shape movement
 */
export function lMovement(
  piece: Piece,
  board: Board,
  modes: SweepModes
): Array<Array<Coords>> {
  // TODO - this is gross, make it better somehow
  const targetCoords = [
    { row: piece.coords.row - 2, col: piece.coords.col - 1 },
    { row: piece.coords.row - 2, col: piece.coords.col + 1 },
    { row: piece.coords.row - 1, col: piece.coords.col - 2 },
    { row: piece.coords.row - 1, col: piece.coords.col + 2 },
    { row: piece.coords.row + 1, col: piece.coords.col - 2 },
    { row: piece.coords.row + 1, col: piece.coords.col + 2 },
    { row: piece.coords.row + 2, col: piece.coords.col - 1 },
    { row: piece.coords.row + 2, col: piece.coords.col + 1 },
  ];

  const validCoords: Array<Array<Coords>> = [];
  const validAttacks: Array<Array<Coords>> = [];
  const side = getSideFromPiece(piece.type);

  targetCoords.forEach((square) => {
    const newRow = square.row;
    const newCol = square.col;

    // check row and col out of bounds
    if (newRow < 0 || newRow > 7 || newCol < 0 || newCol > 7) {
      return;

      // check square is free
    } else if (board[newRow][newCol] === 0) {
      validCoords.push([{ row: newRow, col: newCol }]);

      // check square contains opponent piece
    } else if (
      board[newRow][newCol] &&
      !isPieceOwner(board[newRow][newCol], side)
    ) {
      validCoords.push([{ row: newRow, col: newCol }]);
      validAttacks.push([{ row: newRow, col: newCol }]);
    }
  });
  return modes?.onlyAttacks ? validAttacks : validCoords;
}

/**
 * Update row and col given the direction
 */
export function updateCoords(
  coords: Coords,
  direction: Direction,
  range: number
): Coords {
  const row =
    direction.plusRow === undefined
      ? coords.row
      : direction.plusRow
      ? coords.row + range
      : coords.row - range;
  const col =
    direction.plusCol === undefined
      ? coords.col
      : direction.plusCol
      ? coords.col + range
      : coords.col - range;
  return { row, col };
}

/**
 *  Given a direction and a piece, return valid squares the piece can move to in that direction
 */
export function sweep(
  piece: Piece,
  board: Board,
  move: Move,
  direction: Direction,
  modes?: SweepModes
): Array<Coords> {
  const validCoords: Array<Coords> = [];
  let attack = false;

  const side = getSideFromPiece(piece.type);

  let newRow = 0;
  let newCol = 0;
  for (let i = 1; i <= move.distance; i++) {
    // update row and col given the direction
    const coords = updateCoords(piece.coords, direction, i);
    newRow = coords.row;
    newCol = coords.col;

    // check row and col out of bounds
    if (newRow < 0 || newRow > 7 || newCol < 0 || newCol > 7) {
      break;

      // check square is free
    } else if (board[newRow][newCol] === 0) {
      // excludes pawn diagonal movement
      if (!move.onlyAttack) {
        validCoords.push({ row: newRow, col: newCol });
      }

      // includes pawns diagonal movement if counting pawn attacks as valid
      if (move.onlyAttack && modes?.pawnAttack) {
        validCoords.push({ row: newRow, col: newCol });
        attack = true;
        break;
      }

      // check square contains opponent piece
    } else if (
      board[newRow][newCol] &&
      !isPieceOwner(board[newRow][newCol], side)
    ) {
      if (!move.noAttack) {
        // only excludes pawn
        validCoords.push({ row: newRow, col: newCol });
        attack = true;
      }

      // unless we are on through king mode and this is the king, stop the sweep
      if (
        !(
          modes?.throughKing &&
          (board[newRow][newCol] === PieceType.KING_BLACK ||
            board[newRow][newCol] === PieceType.KING_WHITE)
        )
      ) {
        break;
      }

      // check square contains friendly piece
    } else if (
      board[newRow][newCol] &&
      isPieceOwner(board[newRow][newCol], side)
    ) {
      if (modes?.friendlyFire) {
        validCoords.push({ row: newRow, col: newCol });
      }
      break;
    }
  }

  if (modes?.onlyAttacks) {
    return attack ? validCoords : [];
  }

  return validCoords;
}

export function getOtherSide(side: Side): Side {
  return side === Side.BLACK ? Side.WHITE : Side.BLACK;
}

export function isPieceOwner(piece: number, side: Side): boolean {
  return (
    (piece < 0 && side === Side.WHITE) || (piece > 0 && side === Side.BLACK)
  );
}

/**
 *  Get all unique piece types a given player has on the board, does not include kings
 */
export function getPieceTypes(board: Board, side: Side): Array<PieceType> {
  return [...new Set(flatten(board as number[][]))] // TODO - find out if this cast is dangerous
    .filter(
      (square) =>
        isPieceOwner(square, side) &&
        !(square === PieceType.KING_BLACK || square === PieceType.KING_WHITE)
    );
}

/**
 *  Get coords of a given player's king
 */
export function findKing(board: Board, side: Side): Coords {
  let king: Coords;
  board.map((row, rowNum) =>
    row.map((square, colNum) => {
      if (
        (side === Side.BLACK && square === PieceType.KING_BLACK) ||
        (side === Side.WHITE && square === PieceType.KING_WHITE)
      ) {
        king = { row: rowNum, col: colNum };
      }
    })
  );

  if (!king) {
    throw new Error(`There is no ${side} king`);
  }
  return king;
}

/**
 *  Make sure player isnt putting themselves in check or checkmate with a move
 */
export function isMoveSafe(
  state: GameState,
  piece: Piece,
  newCoords: Coords
): boolean {
  if (getCastlingPairMove(state, piece, newCoords)) {
    return true;
  }

  const endState = checkEndState(
    makeMove(state.board, piece.coords, newCoords),
    state.turn
  );
  return endState !== EndCondition.CHECK && endState !== EndCondition.CHECKMATE;
}

/**
 *  Makes a move on the board
 */
export function makeMove(
  board: Board,
  oldCoords: Coords,
  newCoords: Coords
): Board {
  const newPiece = {
    type: board[oldCoords.row][oldCoords.col],
    coords: newCoords,
  };
  const boardAdd = addPiece(board, newPiece);
  return removePiece(boardAdd, oldCoords);
}

export function addPiece(board: Board, addedPiece: Piece): Board {
  return board.map((row, rowIndex) =>
    row.map((piece, colIndex) =>
      rowIndex === addedPiece.coords.row && colIndex === addedPiece.coords.col
        ? addedPiece.type
        : piece
    )
  );
}

export function removePiece(board: Board, coords: Coords): Board {
  return board.map((row, rowIndex) =>
    row.map((piece, colIndex) =>
      rowIndex === coords.row && colIndex === coords.col ? 0 : piece
    )
  );
}

export function getSideFromPiece(piece: PieceType): Side {
  return piece > 0 ? Side.BLACK : Side.WHITE;
}

/**
 *  Checks if a pawn has made it to the opposite side of the board
 */
export function canUpgradePawn(board: Board, coords: Coords) {
  const { row, col } = coords;
  const piece: Piece = {
    type: board[row][col],
    coords: { row, col },
  };
  return (
    (piece.type === PieceType.PAWN_BLACK && piece.coords.row === 7) ||
    (piece.type === PieceType.PAWN_WHITE && piece.coords.row === 0)
  );
}

/**
 *  Check pairs player can castle with given a current board
 */
export function getValidCastleOptions(
  board: Board,
  side: Side,
  options: CastlingOptions
): Array<CastlingPair> {
  if (
    (side === Side.BLACK && !options.black.length) ||
    (side === Side.WHITE && !options.white.length) ||
    checkEndState(board, side) === EndCondition.CHECK
  ) {
    return [];
  }

  const colRanges = {
    left: [1, 2, 3],
    right: [5, 6],
  };
  const row = side === Side.BLACK ? 0 : 7;
  const opponentMoves = allMoves(board, getOtherSide(side), false);

  return Object.keys(colRanges)
    .filter((castlingSide) => {
      return (colRanges[castlingSide] as number[])
        .map((col) => {
          if (board[row][col]) {
            return false;
          }

          // king does not pass through or end up on the first column so we don't check for an attack on that square
          if (
            col !== 1 &&
            opponentMoves.find((square) => checkCoords(square, row, col))
          ) {
            return false;
          }

          return true;
        })
        .every((canCastleThrough) => {
          return canCastleThrough;
        });
    })
    .map((castlingSide) => {
      const rookCol = castlingSide === "left" ? 0 : 7;
      return {
        king: { row, col: 4 },
        rook: { row, col: rookCol },
      };
    });
}

/**
 *  Check if a king or rook is being moved and if so mark them as such
 *
 *  @var castlingInfo object tracking which pieces that can be used for castling have moved
 */
export function updateCastlingOptions(
  board: Board,
  pieceCoords: Coords,
  castlingOptions: CastlingOptions
): CastlingOptions {
  const piece = getPieceFromBoardCoords(board, pieceCoords);

  if (!isMovingKingOrRook(piece)) {
    return castlingOptions;
  }

  switch (getSideFromPiece(piece.type)) {
    case Side.WHITE: {
      return {
        ...castlingOptions,
        white: castlingOptions.white.filter(
          (option) =>
            !(
              compareCoords(option.rook, piece.coords) ||
              compareCoords(option.king, piece.coords)
            )
        ),
      };
    }
    case Side.BLACK: {
      return {
        ...castlingOptions,
        black: castlingOptions.black.filter(
          (option) =>
            !(
              compareCoords(option.rook, piece.coords) ||
              compareCoords(option.king, piece.coords)
            )
        ),
      };
    }
  }
}

/**
 *  Check if the given piece coordinates match the given row and column
 */
export function checkCoords(pieceCoords: Coords, row: number, col: number) {
  return pieceCoords.row === row && pieceCoords.col === col;
}

export function flipTurn(currentTurn: Side) {
  return currentTurn === Side.BLACK ? Side.WHITE : Side.BLACK;
}

/** Returns the details we need to note a potential in passing attack if the player is moving a pawn two places */
export function updateInPassingChance(
  selectedPiece: Piece,
  destination: Coords
): InPassingChance {
  const { row, col } = destination;
  // if we are moving a pawn 2 spaces forward, make note of a potential in passing attack
  if (
    selectedPiece.type === PieceType.PAWN_BLACK &&
    selectedPiece.coords.col === col &&
    row === 3
  ) {
    return {
      type: selectedPiece.type,
      attackSquare: { row: 2, col },
      pieceSquare: { row, col },
    };
  } else if (
    selectedPiece.type === PieceType.PAWN_WHITE &&
    selectedPiece.coords.col === col &&
    row === 4
  ) {
    return {
      type: selectedPiece.type,
      attackSquare: { row: 5, col },
      pieceSquare: { row, col },
    };
  }
  throw new Error(
    `Updating passing attack info when a pawn has not been moved forward two places`
  );
}

export function isMovingPawnTwoPlaces(
  selectedPiece: Piece,
  destination: Coords
): boolean {
  const { row, col } = destination;
  return (
    (selectedPiece.type === PieceType.PAWN_BLACK &&
      selectedPiece.coords.col === col &&
      row === 3) ||
    (selectedPiece.type === PieceType.PAWN_WHITE &&
      selectedPiece.coords.col === col &&
      row === 4)
  );
}

export function isPerformingInPassingAttack(
  piece: Piece,
  inPassingChance: InPassingChance,
  destination: Coords
): boolean {
  const { row, col } = destination;
  return (
    isMovingPawn(piece) &&
    inPassingChance?.attackSquare.row === row &&
    inPassingChance?.attackSquare.col === col
  );
}

export function isMovingPawn(piece: Piece): boolean {
  return (
    piece.type === PieceType.PAWN_WHITE || piece.type === PieceType.PAWN_BLACK
  );
}

export function isMovingKingOrRook(piece: Piece): boolean {
  return (
    piece.type === PieceType.ROOK_WHITE ||
    piece.type === PieceType.ROOK_BLACK ||
    piece.type === PieceType.KING_WHITE ||
    piece.type === PieceType.KING_BLACK
  );
}

export function getPieceFromBoardCoords(board: Board, coords: Coords): Piece {
  const { row, col } = coords;
  return {
    type: board[row][col],
    coords: coords,
  };
}

export function performCastlingMove(
  state: GameState,
  castlingPair: CastlingPair
): GameState {
  const newState = { ...state };

  // calculate new coordinates of king and rook
  const direction = castlingPair.rook.col < castlingPair.king.col ? -1 : 1;
  const newKingCoords = {
    row: castlingPair.king.row,
    col: castlingPair.king.col + 2 * direction,
  };
  const newRookCoords = {
    row: castlingPair.king.row,
    col: castlingPair.king.col + direction,
  };

  // move rook on board and mark piece as having moved
  newState.castlingOptions = updateCastlingOptions(
    newState.board,
    castlingPair.rook,
    newState.castlingOptions
  );
  newState.board = makeMove(newState.board, castlingPair.rook, newRookCoords);

  // move king on board and mark piece as having moved
  newState.castlingOptions = updateCastlingOptions(
    newState.board,
    castlingPair.king,
    newState.castlingOptions
  );
  newState.board = makeMove(newState.board, castlingPair.king, newKingCoords);

  newState.turn = flipTurn(newState.turn);

  return newState;
}

export function compareCoords(a: Coords, b: Coords): boolean {
  return a.row === b.row && a.col === b.col;
}

export function getCastlingPairMove(
  state: GameState,
  selectedPiece: Piece,
  destination: Coords
): CastlingPair {
  const castlingPairs = getValidCastleOptions(
    state.board,
    state.turn,
    state.castlingOptions
  );
  return castlingPairs.find(
    (pair) =>
      (compareCoords(pair.rook, selectedPiece.coords) &&
        compareCoords(pair.king, destination)) ||
      (compareCoords(pair.king, selectedPiece.coords) &&
        compareCoords(pair.rook, destination))
  );
}

export function makeMoveAndUpdateState(
  state: GameState,
  selectedPiece: Piece,
  destination: Coords
): GameState {
  const { row, col } = destination;
  const newState = { ...state };

  // update board/game info
  if (state.board[row][col] !== 0) {
    newState.capturedPieces = [
      ...newState.capturedPieces,
      newState.board[row][col],
    ];
  }

  if (isMovingKingOrRook(selectedPiece)) {
    const castleMove = getCastlingPairMove(
      newState,
      selectedPiece,
      destination
    );

    if (castleMove) {
      return performCastlingMove(state, castleMove);
    }

    newState.castlingOptions = updateCastlingOptions(
      newState.board,
      selectedPiece.coords,
      newState.castlingOptions
    );
  }

  if (isMovingPawnTwoPlaces(selectedPiece, destination)) {
    // this also serves to override the other players in passing attack vulnerability
    newState.inPassingChance = updateInPassingChance(
      selectedPiece,
      destination
    );
  } else {
    // this stuff can not happen if the player is moving a pawn forward two places
    if (
      isPerformingInPassingAttack(
        selectedPiece,
        newState.inPassingChance,
        destination
      )
    ) {
      newState.board = removePiece(
        newState.board,
        newState.inPassingChance.pieceSquare
      );
      newState.capturedPieces = [
        ...newState.capturedPieces,
        newState.inPassingChance.type,
      ];
    }

    // clear inPassingChance since the player has missed the chance, or just performed the attack
    newState.inPassingChance = undefined;
  }

  // make the move on the board
  newState.board = makeMove(newState.board, selectedPiece.coords, destination);

  // check if a pawn has made it to the other end of the board
  // turn flip will be handled after upgrade is complete in upgradePawn()
  if (canUpgradePawn(newState.board, destination)) {
    newState.upgrade = {
      canUpgrade: true,
      coords: destination,
    };
  } else {
    newState.turn = flipTurn(newState.turn);
  }

  newState.endCondition = checkEndState(newState.board, newState.turn);
  return newState;
}

export function getValidMovesIncludingInPassingAndCastling(
  piece: Piece,
  state: GameState
): Array<Coords> {
  // if there is an opportunity for an inPassing attack and player is moving a pawn
  // add dummy piece to board, othewise use board as is
  const modifiedBoard =
    state.inPassingChance && isMovingPawn(piece)
      ? addPiece(state.board, {
          type: state.inPassingChance.type,
          coords: state.inPassingChance.attackSquare,
        })
      : state.board;

  const movesWithInPassing = flatten(getValidMoveCoords(piece, modifiedBoard));

  if (isMovingKingOrRook(piece)) {
    const castlingPairs = getValidCastleOptions(
      state.board,
      state.turn,
      state.castlingOptions
    );

    const castlingMoves = castlingPairs.map((pair) =>
      piece.type === PieceType.KING_BLACK || piece.type === PieceType.KING_WHITE
        ? pair.rook
        : pair.king
    );

    return [...movesWithInPassing, ...castlingMoves];
  }
  return movesWithInPassing;
}

export function upgradePawn(state: GameState, piece: PieceType): GameState {
  const newPiece = {
    type: state.turn === Side.WHITE ? -1 * piece : piece,
    coords: state.upgrade.coords,
  };

  return {
    ...state,
    board: addPiece(state.board, newPiece),
    upgrade: undefined,
    turn: flipTurn(state.turn),
    endCondition: checkEndState(state.board, state.turn),
  };
}
