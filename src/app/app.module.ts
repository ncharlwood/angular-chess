import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BoardComponent } from "./game/board/board.component";
import { PieceComponent } from "./game/piece/piece.component";
import { CapturedPiecesComponent } from "./game/captured-pieces/captured-pieces.component";
import { GameInfoComponent } from "./game/game-info/game-info.component";
import { GameLoadComponent } from "./game/game-load/game-load.component";
import { GameComponent } from "./game/game.component";

@NgModule({
  declarations: [
    AppComponent,
    PieceComponent,
    CapturedPiecesComponent,
    GameComponent,
    GameInfoComponent,
    GameLoadComponent,
    BoardComponent,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
