import { compareCoords, getNewState, getValidMovesIncludingInPassingAndCastling, isMoveSafe, makeMoveAndUpdateState, upgradePawn } from '../app/game-helpers';
import { GameState } from 'src/app/types.model';
import { isJoinRoomMessage, isMoveMessage, isUpgradeMessage, JoinRoomMessage, MoveMessage, UpgradeMessage } from '../app/services/message.types';

var WebSocketServer = require('websocket').server;
var http = require('http');
var uuid = require("uuid");

// currently conected users
var clients: Clients = {};
var rooms: Rooms = {};

/** Maps client uuid to connection object */
interface Clients {
  [uuid: string]: Client;
}

interface Client {
  connection: any;
  code?: Code;
}

/** Maps room code to players in room with their colours*/
interface Rooms {
  [code: string]: Room;
}

interface Room {
  black?: Uuid; // strings are uuids
  white?: Uuid;
}

type Code = string;
type Uuid = string;

var state: GameState = getNewState();

const webSocketsServerPort = 1337;
const server = http.createServer();

server.listen(webSocketsServerPort, function() {
  console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

// create the server
const wsServer = new WebSocketServer({httpServer: server});

// WebSocket server
wsServer.on('request', function(request: any) {
  var connection = request.accept(null, request.origin);

  var new_uuid = uuid.v4();
  clients[new_uuid] = {
    connection
  };

  var connMsg = "Established connection " + new_uuid;
  console.log(connMsg);
  sendMessage(connection, JSON.stringify(connMsg));

  connection.on('message', function(message: any) {
    if (message.type !== 'utf8') {
      console.log("Message recieved is not UTF8, is ", message.type);
      return
    }

    try {
      var info = JSON.parse(message.utf8Data);
    } catch (e) {
      console.log("Message recieved is not JSON");
      return
    }

    const code = getClientRoom(new_uuid);
    if (!code && isJoinRoomMessage(info)) {
      const colour = handleJoinMessage(info, new_uuid);
      sendMessage(this, JSON.stringify(
        {
          type: 'connect',
          colour
        }
      ));
    }

    var isValid: boolean;
    if (!isClientTurn(state, new_uuid)) {
      isValid = false;
    } else if (isMoveMessage(info)) {
      isValid = handleMoveMessage(info);
    } else if (isUpgradeMessage(info)) {
      isValid = handleUpgradeMessage(info)
    }

    if (isValid) {
      broadcast(info, new_uuid);
    }
  });
});

function broadcast(message: any, uuid: Uuid) {
  const code = getClientRoom(uuid);
  Object.values(rooms[code]).forEach(
    uuid => sendMessage(clients[uuid].connection, JSON.stringify(message))
  );
}

function isClientTurn(state: GameState, uuid: Uuid): boolean {
  const code = getClientRoom(uuid);
  return (state.turn === 'black' && rooms[code]?.black === uuid)
    || (state.turn === 'white' && rooms[code]?.white === uuid);
}

function handleMoveMessage(message: MoveMessage): boolean {
  const piece = message.piece;
  const destination = message.toSquare;

  const validMoves = getValidMovesIncludingInPassingAndCastling(piece, state);

  const canMakeMove = validMoves.find(coords => compareCoords(coords, destination))
    ? isMoveSafe(state, piece, destination)
    : false;

  if (canMakeMove) {
    state = makeMoveAndUpdateState(state, piece, destination)
    return true;
  }

  return false;
}

function handleUpgradeMessage(message: UpgradeMessage): boolean {
  const upgradeType = message.upgrade;

  state = upgradePawn(state, upgradeType);
  return true;
}

function handleJoinMessage(message: JoinRoomMessage, uuid: Uuid): 'black' | 'white' {
  const { code } = message;
  clients[uuid] = {
    ...clients[uuid],
    code,
  }

  if (rooms[code]?.white) {
    rooms[code] = {
      ...rooms[code],
      black: uuid,
    };
    return 'black';

  } else if (rooms[code]?.black) {
    rooms[code] = {
      ...rooms[code],
      white: uuid,
    };
    return 'white';

  } else {
    const colour = Math.random() < 0.5 ? 'black' : 'white';

    rooms[code] = {
      black: colour === 'black' ? uuid : undefined,
      white: colour === 'white' ? uuid : undefined,
    };
    return colour;
  }  
}

function getClientRoom(uuid: Uuid): Code|undefined {
  return clients[uuid]?.code;
}

function sendMessage(connection: any, message: any) {
  connection.send(message);
}
