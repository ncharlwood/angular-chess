import { Board, EndCondition, Side, Piece, PieceType, Move, Movement, Coords, Direction, SweepModes, CastlingOptions, GameState } from '../app/types.model';
import { checkEndState, findKing, makeMove, addPiece, getPieceTypes, sweep, updateCoords, getNewState, getStartingCastlingOptions, performCastlingMove } from '../app/game-helpers';

function makeBoard(pieces: ReadonlyArray<Piece>): Board {
    const board: Board = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
    ];

    return pieces.reduce((accBoard, piece) => addPiece(accBoard, piece), board);
}

function makePiece(pieceType: PieceType, row: number, col: number): Piece {
    return {
        type: pieceType,
        coords: { row, col }
    };
}

describe('kingFunctions', () => {

    let board: Board;
    beforeEach(() => {
        board = [
            [0, 6],
            [-6, 0],
        ];
    });

    it('should find black king', () => {
        expect(findKing(board, Side.BLACK)).toEqual({row: 0, col : 1});
    });

    it('should find white king', () => {
        expect(findKing(board, Side.WHITE)).toEqual({row: 1, col : 0});
    });
});

describe('end game state', () => {
    it('pawn should attack king diagonally and put in check', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.PAWN_BLACK, 6, 1),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.CHECK);
    });
    it('pawn should not put king in check when king is directly infront', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.PAWN_BLACK, 6, 0),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.NONE);
    });
    it('player should be able to block check to escape check', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.ROOK_WHITE, 5, 5),
            makePiece(PieceType.ROOK_BLACK, 6, 7),
            makePiece(PieceType.ROOK_BLACK, 7, 7),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.CHECK);
    });
    it('player should be able to take attacking piece to escape check', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.BISHOP_WHITE, 5, 5),
            makePiece(PieceType.ROOK_BLACK, 6, 7),
            makePiece(PieceType.ROOK_BLACK, 7, 7),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.CHECK);
    });
    it('knight should be able to put king in check', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.KNIGHT_BLACK, 5, 1),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.CHECK);
    });
    it('diagonal of pawn should not be conidered a safe move for escaping checkmate', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.BISHOP_BLACK, 4, 3),
            makePiece(PieceType.PAWN_BLACK, 5, 1),
            makePiece(PieceType.BISHOP_BLACK, 5, 3),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.CHECKMATE);
    });
    it('diagonal of pawn should not be conidered a safe move for escaping stalemate', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.KNIGHT_BLACK, 4, 2),
            makePiece(PieceType.PAWN_BLACK, 5, 1),
            makePiece(PieceType.BISHOP_BLACK, 5, 3),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.STALEMATE);
    });

    it('check standard moves attacking adjacent to king render ', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.ROOK_BLACK, 5, 1),
            makePiece(PieceType.ROOK_BLACK, 6, 6),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.STALEMATE);
    });

    it('diagonal of pawn should not be considered valid move for king', () => {
        const board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 7),
            makePiece(PieceType.PAWN_BLACK, 5, 1),
            makePiece(PieceType.PAWN_BLACK, 5, 2),
            makePiece(PieceType.PAWN_BLACK, 6, 2),
            makePiece(PieceType.KING_WHITE, 7, 0),
        ]);
        expect(checkEndState(board, Side.WHITE)).toEqual(EndCondition.STALEMATE);
    });
});

describe('makeMove', () => {
    it('board should be updated properly', () => {
        const board = makeBoard([makePiece(PieceType.QUEEN_BLACK, 0, 0)]);
        const boardAfter = makeBoard([makePiece(PieceType.QUEEN_BLACK, 3, 3)]);

        const moveStart = { row: 0, col: 0 };
        const moveEnd = { row: 3, col: 3 };
        expect(makeMove(board, moveStart, moveEnd)).toEqual(boardAfter);
    });
});

describe('getPieceTypes', () => {
    let board: Board;

    // done before each it
    beforeEach(() => {
        board = makeBoard([
            makePiece(PieceType.KING_BLACK, 0, 0),
            makePiece(PieceType.KNIGHT_BLACK, 0, 1),
            makePiece(PieceType.BISHOP_BLACK, 0, 2),
            makePiece(PieceType.ROOK_BLACK, 0, 3),
            makePiece(PieceType.PAWN_BLACK, 0, 4),
            makePiece(PieceType.PAWN_BLACK, 0, 5),
        ]);
    });

    it('should not include king piece or any duplicates', () => {
        const pieceTypes = [
            PieceType.KNIGHT_BLACK,
            PieceType.BISHOP_BLACK,
            PieceType.ROOK_BLACK,
            PieceType.PAWN_BLACK,
        ];
        expect(getPieceTypes(board, Side.BLACK)).toEqual(pieceTypes);
    });
});

describe('updateCoords', () => {
    let coords: Coords;
    let iter: number;

    beforeEach(() => {
        coords = {
            row: 4,
            col: 4,
        };

        iter = 3;
    });

    it ('both plus', () => {
        const direction: Direction = {
            plusRow: true,
            plusCol: true
        };

        const newCoords: Coords = {
            row: coords.row + iter,
            col: coords.col + iter,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });

    it ('both minus', () => {
        const direction: Direction = {
            plusRow: false,
            plusCol: false
        };
        const newCoords: Coords = {
            row: coords.row - iter,
            col: coords.col - iter,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });

    it ('plus row', () => {
        const direction: Direction = {
            plusRow: true,
        };
        const newCoords: Coords = {
            row: coords.row + iter,
            col: coords.col,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });

    it ('minus row', () => {
        const direction: Direction = {
            plusRow: false,
        };
        const newCoords: Coords = {
            row: coords.row - iter,
            col: coords.col,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });

    it ('plus col', () => {
        const direction: Direction = {
            plusCol: true
        };
        const newCoords: Coords = {
            row: coords.row,
            col: coords.col + iter,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });

    it ('minus col', () => {
        const direction: Direction = {
            plusCol: false
        };
        const newCoords: Coords = {
            row: coords.row,
            col: coords.col - iter,
        };
        expect(updateCoords(coords, direction, iter)).toEqual(newCoords);
    });
});

describe('performCastlingMove', () => {
    let board: Board;

    function getBlackPieces() {
        return [
            makePiece(PieceType.ROOK_BLACK, 0, 0),
            makePiece(PieceType.KING_BLACK, 0, 4),
            makePiece(PieceType.ROOK_BLACK, 0, 7),
        ];
    }
    function getWhitePieces() {
        return [
            makePiece(PieceType.ROOK_WHITE, 7, 0),
            makePiece(PieceType.KING_WHITE, 7, 4),
            makePiece(PieceType.ROOK_WHITE, 7, 7), 
        ];
    }

    describe("should be able to castle on both sides and directions", () => {

        beforeEach(() => {
            board = makeBoard([
                ...getBlackPieces(),
                ...getWhitePieces(),
            ]);
        })

        const tests = [
            {
                case: 'black left',
                pair: {
                    rook: { row: 0, col: 0 },
                    king: { row: 0, col: 4 },
                },
                turn: Side.BLACK,
                expectedBoard: makeBoard([
                    ...getWhitePieces(),
                    makePiece(PieceType.KING_BLACK, 0, 2),
                    makePiece(PieceType.ROOK_BLACK, 0, 3),
                    makePiece(PieceType.ROOK_BLACK, 0, 7),
                ]),
            },
            {
                case: 'black right',
                pair: {
                    king: { row: 0, col: 4 },
                    rook: { row: 0, col: 7 },
                },
                turn: Side.BLACK,
                expectedBoard: makeBoard([
                    ...getWhitePieces(),
                    makePiece(PieceType.ROOK_BLACK, 0, 0),
                    makePiece(PieceType.ROOK_BLACK, 0, 5),
                    makePiece(PieceType.KING_BLACK, 0, 6),
                ]),
            },
            {
                case: 'white left',
                pair: {
                    rook: { row: 7, col: 0 },
                    king: { row: 7, col: 4 },
                },
                turn: Side.WHITE,
                expectedBoard: makeBoard([
                    ...getBlackPieces(),
                    makePiece(PieceType.KING_WHITE, 7, 2),
                    makePiece(PieceType.ROOK_WHITE, 7, 3),
                    makePiece(PieceType.ROOK_WHITE, 7, 7),
                ]),
            },
            {
                case: 'white right',
                pair: {
                    king: { row: 7, col: 4 },
                    rook: { row: 7, col: 7 },
                },
                turn: Side.WHITE,
                expectedBoard: makeBoard([
                    ...getBlackPieces(),
                    makePiece(PieceType.ROOK_WHITE, 7, 0),
                    makePiece(PieceType.ROOK_WHITE, 7, 5),
                    makePiece(PieceType.KING_WHITE, 7, 6),
                ]),
            },
        ];

        tests.forEach(test => {
            it(`should allow ${test.case} castle`, () => {

                const state = {
                    ...getNewState(),
                    turn: test.turn,
                    board,
                };

                const expectedCastlingOptions = {
                    ...state.castlingOptions,
                    [test.turn]: [],
                };
    
                const newState = performCastlingMove(state, test.pair)
                expect(newState.board).toEqual(test.expectedBoard);
                expect(newState.castlingOptions).toEqual(expectedCastlingOptions);
            });
        })

    });




});


describe('sweep', () => {
    it('should not sweep past range', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 1,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [
            { row: 1, col: 1 },
        ];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should not sweep off the edge of the board adjacent', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: false,
            plusCol: false,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should not sweep off the edge of the board adjacent one movement', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 1,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: false,
            plusCol: false,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should not sweep off the edge of the board far', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 4, 4);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [
            { row: 5, col: 5 },
            { row: 6, col: 6 },
            { row: 7, col: 7 },
        ];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should stop sweeping on square when an opponent piece is hit', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.QUEEN_WHITE, 4, 4)
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [
            { row: 1, col: 1 },
            { row: 2, col: 2 },
            { row: 3, col: 3 },
            { row: 4, col: 4 },
        ];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should stop sweeping before square when an friendly piece is hit', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.ROOK_BLACK, 4, 4)
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [
            { row: 1, col: 1 },
            { row: 2, col: 2 },
            { row: 3, col: 3 },
        ];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should have no moves when friendly piece is adjacent', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.ROOK_BLACK, 1, 1)
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should make one move when opponent piece is adjacent', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.QUEEN_WHITE, 1, 1)
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [
            { row: 1, col: 1 },
        ];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should not include opponent piece square when noAttack flag is set', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.QUEEN_WHITE, 1, 1)
        ]);
        const move: Move = {
            distance: 8,
            noAttack: true,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('should not include moves to empty square when onlyAttack flag is set', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            onlyAttack: true,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction)).toEqual(moves);
    });

    it('pawnAttack mode should do nothing if the onlyttack flag is not set on the move', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: false,
            friendlyFire: false,
            throughKing: false,
            pawnAttack: true,
        };

        const moves = [
            { row: 1, col: 1 },
            { row: 2, col: 2 },
            { row: 3, col: 3 },
            { row: 4, col: 4 },
            { row: 5, col: 5 },
            { row: 6, col: 6 },
            { row: 7, col: 7 },
        ];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });

    it('should include move diagonal of piece if only checking attacks on pawn attack mode', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            onlyAttack: true,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: false,
            friendlyFire: false,
            throughKing: false,
            pawnAttack: true,
        };

        const moves = [
            { row: 1, col: 1 },
        ];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });

    it('when onlyAttacks mode is on, should only include moves if a piece can be captured at some point in the sweep ', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.QUEEN_WHITE, 4, 4),
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: true,
            friendlyFire: false,
            throughKing: false,
            pawnAttack: false,
        };

        const moves = [
            { row: 1, col: 1 },
            { row: 2, col: 2 },
            { row: 3, col: 3 },
            { row: 4, col: 4 },
        ];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });

    it('when onlyAttacks mode is on, should not include moves if a piece cannot be captured at some point in the sweep ', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([piece]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: true,
            friendlyFire: false,
            throughKing: false,
            pawnAttack: false,
        };

        const moves = [];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });

    it('should include moves to capture a friendly piece if friendly fire is on', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.QUEEN_BLACK, 1, 1),
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: false,
            friendlyFire: true,
            throughKing: false,
            pawnAttack: false,
        };

        const moves = [
            { row: 1, col: 1 },
        ];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });

    it('should sweep through a king if on throughKing mode', () => {
        const piece = makePiece(PieceType.QUEEN_BLACK, 0, 0);
        const board = makeBoard([
            piece,
            makePiece(PieceType.KING_WHITE, 4, 4),
        ]);
        const move: Move = {
            distance: 8,
            type: Movement.ORTHOGONAL, // unused in the function, TODO refactor
        };
        const direction = {
            plusRow: true,
            plusCol: true,
        };
        const modes: SweepModes = {
            onlyAttacks: false,
            friendlyFire: false,
            throughKing: true,
            pawnAttack: false,
        };

        const moves = [
            { row: 1, col: 1 },
            { row: 2, col: 2 },
            { row: 3, col: 3 },
            { row: 4, col: 4 }, // wtf
            { row: 5, col: 5 },
            { row: 6, col: 6 },
            { row: 7, col: 7 },
        ];

        expect(sweep(piece, board, move, direction, modes)).toEqual(moves);
    });
});
